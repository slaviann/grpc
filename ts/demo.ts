#!/usr/bin/env node
// import './polyfills'
import * as debug from "debug";
import * as grpc from "grpc";

import {
  Candlestick,
  HistoryReq,
  SubscribeReq,
  Width,
  WidthMap
} from "bitzlato-grpc-node-client/exchange/public/candlestick_pb";
//import {
//  Service,
//  ServiceClient
//} from "bitzlato-grpc-node-client/exchange/public/candlestick_pb_service";
import {
  ScaleBounds,
  SideDepthAndScaleBounds,
  SideReq
} from "bitzlato-grpc-node-client/exchange/public/market-depth_pb";
import { OrderSide } from "bitzlato-grpc-node-client/exchange/public/common_pb";
import axios, { AxiosRequestConfig, AxiosPromise, AxiosResponse, Method } from "axios";
import { ServerResponse } from "http";
import { resolve } from "dns";
import { url } from "inspector";

const restHost = "https://demo.bitzlato.com";
const grpcHost = "https://dm-grpc.bitzlato.com";
const pair = "BCH-BTC";

const log = debug("DemoClient");
//const ohlcvClient = new ServiceClient(grpcHost, grpc.credentials.createSsl());
const token = "9fbfabf0-1372-4b32-bcda-23430fcb95bd";

interface OrderDTO {
  type: string;
  price: number;
  side: string;
  pair: string;
  amount: number;
}

interface WalletDTO {
  cryptocurrency: string;
  balance: string;
  holdBalance: string;
  address: string | null;
  createdAt: number;
  worth: any

}


/* export type Method =
  | 'get' | 'GET'
  | 'delete' | 'DELETE'
  | 'head' | 'HEAD'
  | 'options' | 'OPTIONS'
  | 'post' | 'POST'
  | 'put' | 'PUT'
  | 'patch' | 'PATCH' */

interface OrderDTO { 
  id: number,
  created: number,
  pair: string,
  side: string,
  type: string,
  price: number,
  baseAmount: number,
  quoteAmount: number,
  amountType: string,
  filledAmount: number,
  fee: number,
  status: string
 }


/* const realtimeOhlcv = async (pair: string) => {
  return new Promise((resolve, reject) => {
    const rtReq = new SubscribeReq();
    rtReq.setPair(pair);
    rtReq.setWidth(Width.MINUTE);
    log(`[OHLCV] Subscribe: ${JSON.stringify(rtReq.toObject())}`);

    ohlcvClient.realtime(rtReq, (err, ohlcv: Candlestick) => {
      if (err != null) {
        debug(
          `[OHLCV] err:\nerr.message: ${err.message}\nerr.stack:\n${err.stack}`
        );
        reject(err);
        return;
      }
      log(`[OHLCV] Update: ${JSON.stringify(ohlcv.toObject())}`);
      resolve(ohlcv);
    });
  });
}; */

async function makeRequest<T>(url: string, method: Method = "GET", data: any | null = null, requestToken: string | null = token): Promise<T> {
  return axios
    .request<T>({
      url: url,
      method: method,
      headers: {
        "X-Authorization": requestToken,
        "Content-Type": "application/json; charset=UTF-8"
      },
      data: data
    })
   
    .then(response => response.data);
}

async function main() {
  // get userId
  const userId = await makeRequest<number>(`${restHost}/api/market/v1/private/whoami`)
  // get balance
  const balanceDto = await makeRequest<WalletDTO>(`${restHost}/api/p2p/${userId}/wallets/BTC`)
  const balance = Number(balanceDto.balance)
  console.log(userId)
  console.log(balance)

  // create order
  const createOrderData = {
    "type": "limit",
    "price": 0.02,  
    "side": "buy",
    "pair": `${pair}`,
    "amount": 1 
    }
  const order = await makeRequest<OrderDTO>(`${restHost}/api/market/v1/private/${userId}/orders/`, "POST", createOrderData)
  const orderId = order.id
  console.log(orderId)
  // get list of orders
  const ordersList = await makeRequest(`${restHost}/api/market/v1/private/${userId}/orders/`)
  console.log(ordersList)
  await makeRequest(`${restHost}/api/market/v1/private/${userId}/orders/`, "DELETE")
  

  // const ohlcv = realtimeOhlcv(pair);

  /* const userId = await axios
    .get(`${restHost}/api/market/v1/private/whoami`, {
      headers: {
        "X-Authorization": token,
        "Content-Type": "application/json; charset=UTF-8"
      },
      //transformResponse: [ (data) => { return  data.data; } ]
    }).then( response =>  response.data ) */
  
  /* .then(
      response => {
        console.log(response.data);
      },
      err => {
        console.log(err);
      }
    ); */
  // let res = await createOrder({type: "sell", price: 1, side: "1", pair: "1", amount: 1})

  // await getOrderBook();
  // await getMarketSettings(pair);
  // await ohlcv;
}

main().then(_ => _);

process.on("uncaughtException", err => {
  log(`process on uncaughtException error: ${err}`);
});

process.on("unhandledRejection", err => {
  log(`process on unhandledRejection error: ${err}`);
});
