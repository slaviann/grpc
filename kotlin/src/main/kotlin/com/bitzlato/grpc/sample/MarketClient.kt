package com.bitzlato.grpc.sample

import com.github.bitzlato.grpc.exchange.Common
import com.github.bitzlato.grpc.exchange.candlestick.CandlestickOuterClass
import com.github.bitzlato.grpc.exchange.candlestick.realtime
import com.github.bitzlato.grpc.exchange.market.MarketOuterClass
import com.github.bitzlato.grpc.exchange.market.MarketsGrpc
import com.github.bitzlato.grpc.exchange.market.get
import com.github.bitzlato.grpc.exchange.marketdepth.MarketDepth
import com.github.bitzlato.grpc.exchange.marketdepth.get
import com.github.bitzlato.grpc.exchange.pair.Pair
import com.github.bitzlato.grpc.exchange.pair.ServiceGrpc
import com.github.bitzlato.grpc.exchange.pair.getSingleInfo
import com.google.gson.JsonParser
import io.grpc.ManagedChannelBuilder
import kotlinx.coroutines.runBlocking
import java.util.logging.Logger
import okhttp3.*
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.MediaType.Companion.toMediaTypeOrNull

const val TOKEN = "42f397e2-e769-4a29-89c9-75cbf99cd60a"
const val MARKET_BASE_URL = "http://localhost:8081"
const val P2P_BASE_URL = "http://localhost:8080"
const val GRPC_HOST = "dm-grpc.bitzlato.com"
const val GRPC_PORT = 443
const val PAIR = "BCH-BTC"


class MarketClient {

    private val host = ManagedChannelBuilder.forAddress(GRPC_HOST, GRPC_PORT)
            .useTransportSecurity()
            .build()

    private val marketService = MarketsGrpc.newStub(host)
    private val pairService = ServiceGrpc.newStub(host)
    private val marketDepthService = com.github.bitzlato.grpc.exchange.marketdepth.ServiceGrpc.newStub(host)
    private val candlestickService = com.github.bitzlato.grpc.exchange.candlestick.ServiceGrpc.newStub(host)

    private fun makeRequest(url: String, token: String, body: String? = null, method: String = "GET"): String? {
        val client = OkHttpClient()
                .newBuilder()
                .build()
        val whoAmIRequest = Request.Builder()
                .url(url)
                .method(method, body?.toRequestBody("application/json; charset=utf-8".toMediaTypeOrNull()))
                .header("Content-Type", "application/json; charset=UTF-8")
                .header("X-Authorization", token)
                .build()

        val response = client.newCall(whoAmIRequest).execute()
        return response.body?.string()
    }


    private fun workWithOrders() {

        runBlocking {

            // Берем last (close price)
            val pairInfoRequest = Pair.PairRequest.newBuilder().setPair(PAIR).build()
            val last = pairService.getSingleInfo(pairInfoRequest).run {
                closePrice
            }


            // Берем максимально допустимую дискретность цены
            val marketRequest = MarketOuterClass.MarketRequest.newBuilder().setSymbol(PAIR).build()
            val resp = marketService.get(marketRequest)
            val priceScale = resp.priceScale

            // Берем максимальную дискретность задания объема и минимально допустимый объем ордера для базовой валюты
            val orderAmountScale = resp.base.orderAmountScale
            val orderMinAmount = resp.base.orderMinAmount


            // Берем биды и аски
            val sideReq1 = MarketDepth.SideReq
                    .newBuilder()
                    .setPair(PAIR)
                    .setSide(Common.OrderSide.SELL)
                    .setSortKind(MarketDepth.SortKind.OPPO)
                    .setLimit(1)
                    .build()

            val sideReq2 = MarketDepth.SideReq
                    .newBuilder()
                    .setPair(PAIR)
                    .setSide(Common.OrderSide.BUY)
                    .setSortKind(MarketDepth.SortKind.OPPO)
                    .setLimit(1)
                    .build()

            val bestAsk = marketDepthService.get(sideReq1).getEntries(0).unitPrice
            val bestBid = marketDepthService.get(sideReq2).getEntries(0).unitPrice


            println("""Максимально допустимая дискретность цены price_scale = $priceScale 
Максимальную дискретность задания объема ордера order_amount_scale = $orderAmountScale,
Минимальный объем ордера order_min_amount= $orderMinAmount
Коммиссия с мейкера = ${resp.makerFee}
Коммисся с тейкера = ${resp.takerFee}
Цена закрытия (последняя цена, last) = $last
Лучший bid = $bestBid
Лучший ask = $bestAsk

            """.trimMargin())

        }


        val userId = makeRequest("$MARKET_BASE_URL/api/market/v1/private/whoami",
                TOKEN)!!.toInt()

        val balance = makeRequest("$P2P_BASE_URL/api/p2p/$userId/wallets/BTC",
                TOKEN)!!.let {
            JsonParser.parseString(it).asJsonObject;
        }

        println("Id пользователя: $userId")
        println("Баланс кошелька: ${balance.get("balance").asDouble}")

        //  Создание ордера
        // для поля "price" максимальная дискретность (точность) в переменной price_scale
        // для поля amount максимальная дискретность (точность) в переменной order_amount_scale, минимальное значение в поле order_min_amount
        val orderBody = """{
  "type": "limit",
  "price": 0.02,  
  "side": "buy",
  "pair": "$PAIR",
  "amount": 1 
  }
        """.trimIndent()


        val orderId = makeRequest("$MARKET_BASE_URL/api/market/v1/private/$userId/orders/", TOKEN, orderBody, "POST"
        )!!.let {

            JsonParser.parseString(it).asJsonObject.get("id")
        }
        println("Создан ордер с id: $orderId")


        // получение списка своих ордеров
        val ordersList = makeRequest("$MARKET_BASE_URL/api/market/v1/private/$userId/orders/", TOKEN)!!.let {
            JsonParser.parseString(it).asJsonArray
        }
        println("Список моих ордеров:")
        println(ordersList)

        // удаление ордера
        makeRequest("$MARKET_BASE_URL/api/market/v1/private/$userId/orders/$orderId", TOKEN, null, "DELETE")
        println("Ордер ${orderId} удален")


    }

    private fun subscrOnCandlestick() {
        println("\n Подпишемся на свечи:")

        runBlocking {
            val resp = candlestickService
                    .realtime(CandlestickOuterClass.SubscribeReq
                            .newBuilder().setPair(PAIR)
                            .setWidth(CandlestickOuterClass.Width.MINUTE)
                            .build())

            var counter = 0
            for (r in resp) {
                counter += 1
                if (counter > 2) {
                    resp.cancel()
                    break
                }
                println(r)
            }

        }
    }

    companion object {
        private val logger = Logger.getLogger(MarketClient::class.java.name)

        private var instance = MarketClient()
        /**
         * Greet server. If provided, the first element of `args` is the name to use in the
         * greeting.
         */
        @Throws(Exception::class)
        @JvmStatic
        fun main(args: Array<String>) {
            instance.workWithOrders()
                    //instance.subscrOnCandlestick()
        }
    }
}
